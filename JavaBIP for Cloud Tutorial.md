﻿# JavaBIP for Cloud Tutorial

JavaBIP for Cloud is extended from the legacy JavaBIP running on a local machine. JavaBIP for Cloud is used for clouds such as PaaS and IaaS. This Demo is a microservice used for automatically deploying a web application/microservice. The demo includes: 

* BIPDeployer microservice: 
   * Run on IaaS (Google Compute) or a local machine
   * This microservice in the demo not only calls Heroku APIs but also executes Heroku CLI. Therefore, it needs to run on a virtual machine (such as Google Compute) where BIPDeployer can access the terminal for executing Heroku CLI commands.
* Compute microservice: run on PaaS (Heroku) or a local machine
   * A simple web application/microservice can be run on PaaS (like Heroku) or any PaaS and IaaS


## 1. **Setup BIPDeployer**
   * Get the Heroku authentication token/API key by creating a Heroku account with your e-mail on <https://www.heroku.com/home>, and then go to the Account Setting to get the key as in the photo below:

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.001.png)

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.002.png)


1.1 Clone the source code to your local machine with git at <https://gitlab.inria.fr/tlekhanh/javabip-on-cloud-demo/-/tree/main>:

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.003.png)

   1. HerokuDemo: A demo of JavaBIP-WebApp. 
   1. Notes: Some notes to support users during the running
   1. Change Heroku authentication key with your key associated with your account in BIPDeployer.java

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.004.png)

1.2 Install Heroku for your local machine at <https://devcenter.heroku.com/articles/heroku-cli#download-and-install> and sign in with your created account by “*Heroku login*” at your local machine. Note that the authentication key must be generated with the account.

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.005.png)

1.3. Set the Heroku CLI directory and the file path of a web application/microservice in war format that you want to deploy on Heroku 

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.006.png)

1.4. Open and run JavaBIPonCloudDemo by running BIPDeployer.java. To run it correctly, you must first set up the Tomcat server (<http://tomcat.apache.org/>) and Java JDK. At this time of writing the demo, we are running it with Tomcat 9 and JDK 11 using JRE 1.8 in JavaBIP libraries. 

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.007.png)

1.5. Packages explication

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.008.png)

From the top down to the bottom of the package explorer on the right side are: 

* **compute**: a simple microservice generating a random integer number. 

* **occideployer**: this microservice deploys a Web application/microservice automatically on the Heroku platform. It contains mainly the deployer BIP model and BIP configuration. 

* **monitor**: this is a Web service demo for monitoring requests to servers using JavaBIP.

In this demo, we use **HerokuDemo.war** for deploying 

**Note**: for understanding more about Heroku APIs and CLI commands, its references published at <https://devcenter.heroku.com/articles/heroku-cli> 


## 2. **HerokuDeployer demo**

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.009.png)

Deployer model.

To use BIPDeployer, we use APIs which are using HTTP Get methods. In the demo, we will show you how to deploy a Compute application onto Heroku free container (Dyno) located in Europe or the US with a native Heroku Postgres database addon.

2.1. Enter the following URL

[http://localhost:<your_port>/HerokuDemo/BIPDeployerOCCI?req=deploy&region=us&buildpack=jvm&addon=heroku-postgresql&pushApp=true](http://localhost:8080/HerokuDemo/BIPDeployerOCCI?req=deploy&region=us&buildpack=jvm&addon=heroku-postgresql&pushApp=true).  This URL With **req** is *deploy*, the **region** is *us*, the **build pack** is *JVM* (java virtual machine), the **addon** is *Heroku-Postgres* and **pushApp** is *true*. The last parameter can be set as *false* to skip deploying the app to the deployed container, which takes quite a long time.![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.010.png)

When it is deployed, you will return a JSON file: 

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.011.png)

Open the JSON file you will see the information of container name or web application name, region, and the application URL

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.012.png)

2.2. ` `Open the application on deployed container
   * ` `Follow the URL in the previous step to go to the application. If you can see the content below, the Compute application is successfully deployed on a Heroku container. 

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.013.png)

   * ` `Entering your servlet URL to run the application, we name our servlet in Compute application is *compute*. After request, the server will show the content in JSON format as below:

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.014.png)

2.3. Check the configuration of the deployed container

We can check the container configuration on (<https://dashboard.heroku.com/apps>). On this page, we can see the created container is located in the US location 

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.015.png) 



Dyno plan we created here is free with a free Postgre database add-on plan

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.016.png)

Our builpack is Heroku/jvm

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.017.png)

2.4. Control, monitor, detach application

When an application or microservice is deployed, the JavaBIP engine is still running to control and monitor it as in the BIPDeployer model above. We are currently implementing a few functions for showing in the demo.

- Delete the container: <http://localhost:10463/HerokuDemo/BIPDeployer?req=deleteContainer>
- Detach the JavaBIP engine, which detaches the JavaBIP from controlling the container and leaves the application:  <http://localhost:10463/HerokuDemo/BIPDeployer?req=detach>

We will try to delete the container

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.018.png)

2.5. APIs:
- Start JavaBIP engine: [http://localhost:port/HerokuDemo/BIPDeployer?req=start](about:blank)
- Stop JavaBIP engine: [http://localhost:port/HerokuDemo/BIPDeployer?req=stop](about:blank)
- Restart JavaBIP engine: [http://localhost:port/HerokuDemo/BIPDeployer?req=restart](about:blank)
- Delete container: [http://localhost:port/HerokuDemo/BIPDeployer?req=deleteContainer](about:blank)
- Detach container: [http://localhost:port/HerokuDemo/BIPDeployer?req=detach](about:blank)
- Deploy a web application or microservice [http://localhost:port/HerokuDemo/BIPDeployer?req=deploy&region=us(eu)&buildpack=jvm&addon=heroku-postgresql&pushApp=true(false)](about:blank)

In case of any error in deploying, the server will send back error information in JSON

## 3. **Monitor-Switch demo**

To illustrate the advantages of the Web application developed following the exogenous approach. Look at the package **occimonitor**, which contains a **SwitchConnector** and four different policy scenarios of **MonitorConnector** as described in the paper. Depending on the demand of users, they can change the policy easily by choosing a specific one. After choosing the policy, users update the config in the main file and then run it on server.

`      `The model of the Web application:

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.019.png)

The application is implemented in the package occimonitor. To run this application, right click on **MonitorswitchTest.java** and choose '**Run as/Run on Server**'

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.020.png)

Choose a policy. In this demo, we chose policy **Monitor\_3**:

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.021.png)

Update the config in the main file (e.g. **MonitorswitchTest.java**)

![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.022.png)

The requests to the server are monitored by the JavaBIP framework through BIP connectors as described in the paper.
![](images/Aspose.Words.9ff32fab-3c40-4dc8-bc73-b5adde365d6e.023.png)

