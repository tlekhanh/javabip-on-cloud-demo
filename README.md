# A Demo for Using OCCIware-BIP framework
Click on the following image to watch a video of the demo
[![Watch the video](images/feature-img.png)](https://youtu.be/JBm61st830E)

The demo is presented following below steps:

## 1. Design an application using OCCIwareBIP
  - Create an OCCI extension 
  - Define components and their finite state machine
  - Define BIP connectors, data transfers and properties
  - Choose which policies will be implemented in "MAIN"
  
## 2. Create the configuration model
  - Right-click to the model and choose OCCI Design/Generate Sirius Design
  - In the generated `monitorswitch.design` project, choose Run As/Eclipse Application
  - Follow steps in the video to define instances for each component
  - Convert the config to text file for using in Step 4

## 3. Generate `monitorswitch.connector` project
  - Including classes with JavaBIP annotations for each component and JavaBIP macro for BIP Connectors and data transfers
  - If users update the policies (i.e, change content of kind Specification), we can update this project by Right-click the project --> OCCI Studio / Regenerate OCCI Connector
  - A .xml file which contains the information of the whole design system also be generated for Step 4

Steps 1 to 3 can be done using OCCIware-BIP studio. You can download the project and tutorial [at this OCCIware-BIP repository](https://gitlab.inria.fr/tlekhanh/occiware-bip-studio/-/tree/main)
## 4. Generate input for verifying using iFinder
  - Copy .xml file in Step 3 and the config model in Step 2 in to `GeneratingBIPFile` project.
  - Run it and the artifacts generated in 'output' folder including: BIP models (.bip), invariants (.inv), and properties (.prop)
  - Copy it into iFinder project in Ubuntu 16.04 then run. 
    - If specifying wrong BIP connector, it will be failed to load the model
    - If the result is valid, go to Step 5. Otherwise, recheck the design model
  - In the example, I show a typo error when specifying a BIP connector, so the model is unloadable. After fixing the typo, the model can be loaded as normal.
  **Note:** In the case that users change the policy, they can change directly in the .xml file and re-generate the artifacts for re-verifying

## 5. Implement the Web application
  - Copy generated Java classes with JavaBIP annotations for components and the GlueBuilder into the project
  - Write the content of class functions and the front-end code (if needed)
  - Run the application.
  - If the policies change, use Regenerate OCCI Connector (Step 3) and replace the regenerated JavaBIP macro code into the project to update it.

The guidance for installation can be found at [JavaBIP for Cloud Tutorial.md](https://gitlab.inria.fr/tlekhanh/javabip-on-cloud-demo/-/blob/main/JavaBIP%20for%20Cloud%20Tutorial.md)